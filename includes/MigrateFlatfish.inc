<?php
/**
 * FlatfishMigration abstract base class, extends DynamicMigration
 *
 * Does a lot of the work for the Node and Media subclasses
 *
 */

abstract class MigrateFlatfish extends DynamicMigration {

  protected $config;

  abstract public function initDestination();

  /**
   * Constructor
   *
   * Get Config, Mapping, Source, and Desitination
   * Check for mismatches between source and mappings
   */
  public function __construct(Array $arguments) {
    $this->arguments = $arguments;
    $this->config = flatfish_load_config();

    //if (some error) {
    //  throw new Exception(t('blah'));
    //}
    parent::__construct(MigrateGroup::getInstance('Flatfish'));

    $this->schemaKey = array(
      'id' => array(
        'type' => 'int',
        'length' => 11,
      ),
    );

    $this->initMap();
    $this->initSource();
    $this->initDestination();

    if ('Media' != $this->getBaseName()) {
      $this->buildFieldMappings();
    }

  }

  /**
   * Required by parent for dynamic migrations
   *
   * Construct the machine name
   */
  protected function generateMachineName($class_name = NULL) {
    return strtolower($this->arguments['machine_name']);
  }

  /**
   * Boilerplate getter
   */
  public function getBaseName() {
    return $this->arguments['machine_name'];
  }

  public function initMap() {
    $this->map = new MigrateSQLMap($this->machineName, $this->schemaKey, MigrateDestinationNode::getKeySchema());
  }


  public function initSource() {
    // setup the db conn
    $config = $this->config;

    $conn = $config[$this->getBaseName()]->config;

    Database::addConnectionInfo('flatfish', 'default', array(
      'driver'   => 'mysql',
      'database' => $conn['database'],
      'username' => $conn['username'],
      'password' => $conn['password'],
      'host'     => $conn['host'],
      'prefix'   => $conn['prefix'],
    ));
  }

  public function getSourceFields() {
    $config = $this->config;
    $keys = array_keys($config[$this->getBaseName()]->config[$this->getBaseName()]['fields']);
    return $keys;
  }

  /**
   * Build field mappings. This comes from config, where the mapping (source_field, dest_field) together with callbacks,
   * arguments, etc., are declared
   */
  public function buildFieldMappings() {
    $input_filter = variable_get('flatfish_input_filter', 'full_html');

    foreach ($this->getSourceFields() as $def) {
      if (in_array($def, array('id', 'url', 'menu_index', 'menu_parent'))) {
        $this->addUnmigratedSources(array($def));
        //continue; // skip our key
      }
      else {
        if ('pathauto' == $def && !module_exists('pathauto')) {
          $mapping = $this->addFieldMapping('path', $def);
        }
        else {
          $mapping = $this->addFieldMapping($def, $def);
        }
        // don't processHTML for the path(auto) and title fields
        if (!in_array($def, array('pathauto','title'))) {
          $mapping->arguments(array('format' => $input_filter));
          $mapping->addCallback(array($this, 'processHTML'));
        }
      }
    }
  }

  /**
   * Process the Source's HTML
   *
   * @param $html
   *   The HTML to be processed
   */
  public function processHTML($html) {
    $processor = new HTMLProcessor($this, $html);
    return $processor->execute();
  }

  /**
   * We have to override an entire method just to be able to use our own Mapper class.
   */
  public function addFieldMapping($destination_field, $source_field = NULL) {
    // Warn if duplicate mappings
    if (!is_null($destination_field) && isset($this->fieldMappings[$destination_field])) {
      self::displayMessage(
        t('!name addFieldMapping: !dest was previously mapped, overridden',
        array('!name' => $this->machineName, '!dest' => $destination_field)),
        'warning');
    }
    $mapping = new MigrateFieldMappingAlt($destination_field, $source_field);
    if (is_null($destination_field)) {
      $this->fieldMappings[] = $mapping;
    }
    else {
      $this->fieldMappings[$destination_field] = $mapping;
    }
    return $mapping;
  }

  /**
   * This grabs previously migrated entities (like media), so that we can update references
   * like in the HTMLProcessor
   */
  public function getMappedEntity($source_migrations, $source_keys, $default = NULL) {
    foreach ($source_migrations as $source_migration) {
      $result = $this->handleSourceMigration($source_migrations, $source_keys, $default);
      if ($result) {
        $sourceMigration = Migration::getInstance($source_migration);
        if (!is_callable(array($sourceMigration->destination, 'getEntityType'))) {
          continue;
        }
        $entityType = $sourceMigration->destination->getEntityType();
        return array_shift(entity_load($entityType, array($result)));
      }
    }

    return FALSE;
  }
}

/**
 * Add callbacks to field mappings
 */
class MigrateFieldMappingAlt extends MigrateFieldMapping {
  protected $callbacks = array();

  public function __construct($destination_field, $source_field) {
    parent::__construct($destination_field, $source_field);
  }

  public function addCallback($callback) {
    $this->callbacks += array($callback);
  }
}
