<?php
/**
 * Processes HTML--cleans it and updates hrefs and img tokens
 *
 * Depends on QueryPath and HTMLPurifier
 *
 */

class HTMLProcessor {

  protected $qp; // QueryPath

  protected $migration;

  /**
   * constructor
   *
   * set migration and load QueryPath
   */
  public function __construct($migration, $html = NULL) {
    $this->migration = $migration;

    if ($qp_path = libraries_get_path('querypath')) {
      require_once $qp_path . '/src/qp.php';
    }

    $this->qp = htmlqp($html);
  }

  /**
   * Update Media, internal links, and clean HTML via check_markup().
   */
  public function execute() {
    $this->updateTokens();
    return $this->cleanHTML();
  }

  private function cleanHTML() {
    // snag the HTML
    $html = $this->qp->top('body')->innerHTML();

    // load htmlpurifier and clean it
    if ($hp_path = libraries_get_path('htmlpurifier')) {
      require_once $hp_path . '/library/HTMLPurifier.auto.php';
    }

    $config = HTMLPurifier_Config::createDefault();
    $allowed_tags = array(
      'p',
      'br',
      'b',
      'a[href]',
      'li',
      'ul',
      'ol',
      'h1',
      'h2',
      'h3',
      'i',
      'em',
      'strong',
      'img[src]',
    );
    $config->set('HTML.Allowed', implode(',', $allowed_tags));
    $purifier = new HTMLPurifier($config);
    // rm extra whitespace
    return preg_replace('/\s\s+/', ' ', $purifier->purify($html));
  }

  /**
   * Update tokens in anchor and img tags
   *
   */
  public function updateTokens() {
    $this->processAnchors();
    $this->processImgs();
  }

  /**
   * Replace media tokens in img tags
   */
  private function processImgs() {
    $migration = $this->migration;
    $qp = $this->qp;

    foreach ($qp->top()->find('img') as $img) {
      $src = $img->attr('src');
      $id = trim(array_pop(explode(':', $src)), ']');
      $key = trim(array_shift(explode(':', $src)), '[');
      if (!is_numeric($id)) {
        continue;
      }
      $item = $migration->getMappedEntity(array('Media'), array(array('id' => $id)));

      if ($item) {
        // Filter syntax is just a json_encode()ed array
        $attrs = getimagesize($item->uri);
        $height = ((int)$img->attr('height'))? (int)$img->attr('height'): $attrs[0];
        $width = ((int)$img->attr('width'))? (int)$img->attr('width'): $attrs[1];
        $repl = json_encode(array(
          'type'       => 'media',
          'view_mode'  => 'media_large',
          'fid'        => (int)$item->fid,
          'attributes' => array(
            'alt'      => check_plain($img->attr('alt')),
            'title'    => check_plain($img->attr('title')),
            'height'   => $height,
            'width'    => $width,
          ),
        ));

        $img->replaceWith('[[' . $repl . ']]');
      }
    }
  }

  /**
   * Replace media and link tokens in anchors
   */
  private function processAnchors() {
    $migration = $this->migration;
    $qp = $this->qp;
    foreach ($qp->top()->find('a') as $a) {
      $href = $a->attr('href');
      $id = trim(array_pop(explode(':', $href)), ']');
      $key = trim(array_shift(explode(':', $href)), '[');

      if (!is_numeric($id)) {
        continue;
      }
      // Replace media tokens in anchor tags (ie, downloadable files--pdfs, docs, xls, etc)
      if ('media' == $key) {
        $item = $migration->getMappedEntity(array('Media'), array(array('id' => $id)));
        if ($item) {
          $a->attr('href', file_create_url($item->uri));
        }
      }
      // Replace link tokens
      elseif ("link" == $key) {
        $result = Database::getConnection('default', 'flatfish')
          ->select('links', 'l')
          ->fields('l', array('url', 'map_type', 'map_id'))
          ->condition('id', $id,'=')
          ->execute()
          ->fetchObject();
        if (!$result) {
          $migration->saveMessage("Could not find link:$id, this means your content is wrong via the flatfish gem.  You can try re-running the gem and file a github issue if the problem persists.");
          continue;
        }
        if (-1 == $result->map_id) {
          $a->attr('href', $result->url);
          $migration->saveMessage("It looks like you didn't migrate link:$id.  As a stop-gap we have kept the URL {$result->url}; however, you may want to create a 301 redirect.", MigrationBase::MESSAGE_NOTICE);
          continue;
        }
        $entity = $migration->getMappedEntity(array($result->map_type), array(array('id' => $result->map_id)));
        if ($entity) {
          $a->attr('href', '/' . drupal_get_path_alias("node/" . $entity->nid));
        }
        else {
          $a->attr('href', '/' . $result->url);
          // TODO see if we can create stubs for these instead
          $msg = "We were unable to update link:$id, you can retry running the migration.  As a stop-gap, we have kept the original link {$result->url}.";
          $migration->saveMessage($msg, MigrationBase::MESSAGE_NOTICE);
        }
      }
    }
  }

}
