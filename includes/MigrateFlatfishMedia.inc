<?php
/**
 * MigrateFlatfishMedia class, extends MigrateFlatfish
 * nearly all work is done by the base class
 */
class MigrateFlatfishMedia extends MigrateFlatfish {
  public function __construct(Array $arguments) {
    parent::__construct($arguments);

    $this->description = t('Migrate media files from flatfish.');

    // Basic fields
    $this->addFieldMapping('uid')
      ->defaultValue(1);

    $this->addFieldMapping('value', 'value')
      ->description('The file blob');

    $this->addFieldMapping('destination_file', 'destination_file')
      ->description('The filename');

    // Unmapped destination fields
    $this->addUnmigratedDestinations(array('fid', 'timestamp',
      'destination_dir', 'file_replace', 'preserve_files'));
    // Unmapped source fields
    $this->addUnmigratedSources(array('url', 'id'));
  }

  public function initSource() {
    parent::initSource(); // DB conn
    $query = Database::getConnection('default', 'flatfish')
      ->select('media', 'm')
      ->fields('m', $this->getSourceFields());

    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
  }

  public function initDestination() {
    $this->destination = new MigrateDestinationFile('file', 'MigrateFileBlob');
  }

  public function getSourceFields() {
    $fields = array('url', 'id', 'value', 'destination_file');
    return $fields;
  }
}
