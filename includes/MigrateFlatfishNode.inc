<?php
/**
 * MigrateFlatfishNode class, extends MigrateFlatfish
 * a lot of the works is done by the base class
 */
class MigrateFlatfishNode extends MigrateFlatfish {
  public function __construct(Array $arguments) {
    parent::__construct($arguments);

    $this->description = t('Migrate flatfish nodes into Drupal');
    $this->addFieldMapping('uid')->defaultValue(1);
  }

  /**
   * run during parent constructor
   */
  public function initSource() {
    parent::initSource(); // gets DB conn

    $name = $this->config[$this->getBaseName()]->config[$this->getBaseName()]['machine_name'];
    $query = Database::getConnection('default', 'flatfish')
      ->select($name, 'type')
      ->fields('type', $this->getSourceFields());
    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
  }

  /**
   * run during parent constructor
   */
  public function initDestination() {
    $name = $this->config[$this->getBaseName()]->config[$this->getBaseName()]['machine_name'];
    $this->destination = new MigrateDestinationNode($name);
  }
}
