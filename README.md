# MAGICK!!!

1. *Make sure your Drupal content types exist AND match your Flatfish CSV(s).*
2. Run Flatfish (the ruby gem)!
3. Make sure your database, config.yml, and schema.yml are all accessible to Drupal--the config.yml and schema.yml should be in this module's migrations directory.
4. Make sure you have all of the dependent libraries installed and set up properly.
5. Enable the Flatfish Drupal module.
5. Run the Migration(s). `drush ms; drush mi <my migration(s)>` (FTW)

# IMPORTANT NOTES

* Watch out, not all special variables, classes, functions are clearly marked at this time.
* You can configure the migrated node's input format with the flatfish\_input\_format variable, eg:
```php
# settings.php
$conf['flatfish_input_filter'] = 'panopoly_wysiwyg_text';
```
