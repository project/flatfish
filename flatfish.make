api = 2
core = 7.x

; Libraries
; get a specific version or use a git hash
libraries[spyc][directory_name] = spyc
libraries[spyc][download][type] = git
libraries[spyc][download][url] = git://github.com/mustangostang/spyc.git
libraries[spyc][download][revision] = b7fd7f7

libraries[htmlpurifier][directory_name] = htmlpurifier
libraries[htmlpurifier][download][type] = get
libraries[htmlpurifier][download][url] = http://htmlpurifier.org/releases/htmlpurifier-4.6.0.tar.gz

libraries[querypath][directory_name] = querypath
libraries[querypath][download][type] = git
libraries[querypath][download][url] = git://github.com/technosophos/querypath.git
libraries[querypath][download][revision] = e0895d5
